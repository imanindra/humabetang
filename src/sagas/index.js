import { takeEvery, fork, all } from 'redux-saga/effects';
import authSaga from './authSaga';
import newsSaga from './newsSaga';

export default function* rootSaga() {
  yield all([
      fork(authSaga),
      fork(newsSaga),
  ]);
}
