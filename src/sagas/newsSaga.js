import {call, put, fork, take} from 'redux-saga/effects';
import {
  FETCH_NEWS,
  fetchNewsStart,
  fetchNewsFinish,
  fetchNewsSuccess,
} from '../redux/newslist';
import {setErrorMessage} from '../redux/error';
import {getNews} from '../services/api';
import Storage from '../data/Storage';

function* newsSaga() {
  while (true) {
    const action = yield take(FETCH_NEWS);
    try {
      yield put(fetchNewsStart());
      const token = yield Storage.getToken();
      const newsData = yield call(
        getNews,
        token,
      );

      if(__DEV__) console.log("Data news data is ", newsData);

      yield put(fetchNewsSuccess(newsData))
    } catch (err) {
      console.log('ERROR GET NEWS DATA  ', err);
      fetchNewsFinish(err)
    }
  }
}


export default function* wacher() {
  yield fork(newsSaga);
}
