import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions, TextInput } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../assets'
import Constant from '../constant/constant'
import LinearGradient from 'react-native-linear-gradient';
const { width, height } = Dimensions.get('window');

class HeaderClose extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { Title } = this.props
        return (
            <LinearGradient
                start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                locations={[0.3, 0.5]}
                colors={['#B82925', '#5C1513']}
                style={styles.container} elevation={1}>
                <View style={{ flexDirection: 'row', paddingHorizontal: wp(5), justifyContent: 'center' }}>
                    <Image
                        source={assets.icon_close}
                        style={{
                            alignSelf: 'center',
                            width: wp(4),
                            height: wp(4),
                            position: 'absolute',
                            left: wp(5)
                        }}
                    />
                    <Text
                        style={{ fontSize: 16, color: 'white', alignSelf: 'center', marginLeft: wp(5) }}
                    >
                        {Title}
                    </Text>
                </View>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: hp(7),
        backgroundColor: Constant.COLOR.WHITE,
        shadowColor: "#000000",
        justifyContent: 'center',
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    iconSearch: {
        position: 'absolute',
        left: wp(32),
        zIndex: 1,
        width: wp(5),
        height: wp(5),
        alignItems: 'center'
    },
    iconProfile: {
        width: wp(8),
        height: wp(8),
        alignItems: 'center'
    }
})

export default HeaderClose
