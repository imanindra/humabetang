import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Alert,
    ScrollView,
    FlatList,
    Dimensions
} from 'react-native';
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import Color from '../constant/Color';
import { Actions } from 'react-native-router-flux';
import moment from "moment";
import 'moment/locale/id'
import Font from '../constant/Font';
import assets from '../assets';
import { setView } from '../services/api';


export default class ListBerita extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    clickEventListener(item) {
        Alert.alert(item.title)
    }

    async onSetView() {
        // alert('tes')
        const { id_news } = this.state
        console
        await setView(id_news).then(data => {
            this.setState({

            })
            console.log(':::APAAA', data)
            Actions.DetailBerita({ id_news })
        })
    }

    render() {
        const { dataList } = this.props
        console.log('datalist', dataList)
        return (
            <View style={styles.container}>
                <FlatList style={styles.list}
                    contentContainerStyle={styles.listContainer}
                    data={dataList}
                    horizontal={false}
                    keyExtractor={(item) => {
                        return item.id;
                    }}
                    renderItem={({ item }) => {
                        return (
                            <View style={{ marginBottom: heightPercentageToDP(1) }}>
                                <TouchableOpacity
                                    style={[styles.card]}
                                    // onPress={() => Actions.DetailBerita({ id_news: item.id })}
                                    onPress={() => this.setState({ id_news: item.id }, () => this.onSetView())}
                                >
                                    <Image
                                        style={styles.cardImage}
                                        source={{ uri: item.news_img }} />

                                    <View style={{ paddingHorizontal: widthPercentageToDP(1), flex: 1, justifyContent: 'space-around' }}>
                                        <Text
                                            numberOfLines={2}
                                            style={[Font.text16HevelticaBold, {}]}
                                        >{item.news_title}</Text>
                                        <Text style={[Font.text10UbuntuItalic, { color: '#BD2B27' }]}>{moment(item.created_at).format('LLLL')}</Text>
                                    </View>

                                    <View style={{
                                        flexDirection: 'row',
                                        position: 'absolute',
                                        alignSelf: 'center',
                                        justifyContent: 'center',
                                        right: widthPercentageToDP(5),
                                        bottom: heightPercentageToDP(1.5),
                                    }}>
                                        <Image
                                            source={assets.icon_view}
                                            style={{
                                                resizeMode: 'contain',
                                                width: widthPercentageToDP(4.5),
                                                height: widthPercentageToDP(4.5),
                                            }}
                                        />
                                        <Text style={[Font.text10UbuntuItalic, { textAlignVertical: 'center', marginLeft: 5, color: '#BD2B27' }]}>{item.views_count} Views</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        )
                    }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: heightPercentageToDP(1),
        width: widthPercentageToDP(95),
        alignSelf: 'center',
        flex: 1
    },
    list: {
        //paddingHorizontal: 5,
        // backgroundColor: "#E6E6E6",
    },

    card: {
        flexDirection: 'row',
        borderRadius: heightPercentageToDP(1),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        backgroundColor: 'white',
        elevation: 2,
        flex: 1,
        paddingVertical: heightPercentageToDP(1)
    },
    cardImage: {
        borderRadius: widthPercentageToDP(1.5),
        height: widthPercentageToDP(20),
        width: widthPercentageToDP(25),
        resizeMode: 'stretch',
        alignSelf: 'center',
        marginHorizontal: widthPercentageToDP(2)
    },
    title: {
        fontSize: 20,
        flex: 1,
        color: Color.COLOR.BLACK,
        fontWeight: 'bold',
        // marginLeft: 40
    },
    subTitle: {
        fontSize: 12,
        flex: 1,
        color: "#FFFFFF",
    },
    icon: {
        height: 20,
        width: 20,
    }
});
