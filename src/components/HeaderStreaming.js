import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions, TextInput } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../assets'
import Constant from '../constant/constant'
import Font from '../constant/Font';
const { width, height } = Dimensions.get('window');

class HeaderStreaming extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { Title } = this.props
        return (
            <View style={styles.container} elevation={1}>
                <View style={{ flexDirection: 'row',justifyContent : 'space-between', paddingHorizontal: wp(5) }}>
                    <Image
                        source={assets.icon_logo}
                        style={{
                            width: wp(15),
                            height: hp(5),
                            alignItems: 'center',
                            resizeMode: 'contain'
                        }}
                    />
                    <Text
                        style={[Font.text16HevelticaReg, { color: '#B72824',textAlignVertical : 'center' }]}
                    >{Title.toUpperCase()}</Text>

                    <Image
                        source={assets.icon_profile}
                        style={styles.iconProfile}
                    />

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: hp(7),
        backgroundColor: Constant.COLOR.WHITE,
        shadowColor: "#000000",
        justifyContent: 'center',
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    iconSearch: {
        position: 'absolute',
        left: wp(32),
        zIndex: 1,
        width: wp(5),
        height: wp(5),
        alignItems: 'center'
    },
    iconProfile: {
        width: wp(8),
        height: wp(8),
        alignItems: 'center'
    }
})

export default HeaderStreaming
