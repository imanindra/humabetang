import React from 'react';
import Carousel from 'react-native-banner-carousel';
import { StyleSheet, Image, View, Dimensions, Text, TouchableOpacity } from 'react-native';
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import assets from '../assets';
import Color from '../constant/Color';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import Font from '../constant/Font';

const BannerWidth = Dimensions.get('window').width;

export default class App extends React.Component {
    renderPage(item, index) {
        return (
            <TouchableOpacity
                onPress={() => Actions.DetailBerita({ id_news: item.id })}
                key={index}>
                <Image style={{
                    borderTopLeftRadius: widthPercentageToDP(2),
                    borderTopRightRadius: widthPercentageToDP(2),
                    width: widthPercentageToDP(95),
                    height: heightPercentageToDP(25)
                }}
                    source={{ uri: item.news_img }} />
                <LinearGradient
                    start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                    locations={[0, 1]}
                    colors={['#B82925', '#5C1513']}
                    style={{
                        height: heightPercentageToDP(11.5),
                        width: widthPercentageToDP(95),
                        borderBottomRightRadius: 5,
                        borderBottomLeftRadius: 5,

                    }}>
                    <Text
                        numberOfLines={2}
                        style={[Font.text20HevelticaReg, {
                            color: 'white',
                            marginVeritcaltextAlign: 'center',
                            textAlign: 'center',
                            marginTop: heightPercentageToDP(2),
                            width: widthPercentageToDP(95),
                            paddingHorizontal: widthPercentageToDP(5)
                        }]}>
                        {item.news_title}
                    </Text>
                </LinearGradient>
            </TouchableOpacity>
        );
    }

    render() {
        const { dataSlide } = this.props
        return (
            <View style={styles.container}>
                <Carousel
                    autoplay
                    autoplayTimeout={5000}
                    loop
                    index={0}
                    pageIndicatorStyle={{ backgroundColor: '#620300' }}
                    activePageIndicatorStyle={{ backgroundColor: 'red' }}
                    pageSize={BannerWidth}
                >
                    {dataSlide.map((image, index) => this.renderPage(image, index))}
                </Carousel>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: widthPercentageToDP(95),
        marginHorizontal: widthPercentageToDP(5),
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,

        // paddingHorizontal: widthPercentageToDP(5),
        backgroundColor: Color.COLOR.WHITE,
        justifyContent: 'center',
        alignSelf: 'center'
    },
});