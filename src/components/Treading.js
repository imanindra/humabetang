import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions, TextInput, FlatList, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const { width, height } = Dimensions.get('window');
import LinearGradient from 'react-native-linear-gradient';

import { getCategoryShow } from '../services/api';
import Storage from '../data/Storage';
import Font from '../constant/Font';
import { Actions } from 'react-native-router-flux';

const styles = StyleSheet.create({
    borderView: {
        backgroundColor: 'red',
        padding: wp(1),
        width: wp(21),
        borderRadius: wp(5),
        marginHorizontal: wp(2),
    }
})


class Treading extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataCategory: []
        };
    }

    async componentDidMount() {
        const token = await Storage.getToken()
        await getCategoryShow(token).then(data => {
            var newData = data.data
            newData.push({
                id: 0,
                news_categories_name: 'Terbaru',
                news_categories_app_img: "Terbaru",
                news_categories_web_img: '',
                news_categories_status_delete: '',
                created_at: '',
                updated_at: ''
            }),
                this.setState({
                    dataCategory: newData.reverse()
                })
        })
    }

    render() {
        const { dataCategory } = this.state
        return (
            <View style={{ alignItems: 'center' }}>
                <FlatList
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    data={dataCategory}
                    renderItem={({ item }) =>
                        <TouchableOpacity
                            onPress={() => Actions.Home({ id_categorys: item.id })}
                        >
                            <LinearGradient
                                start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                                locations={[0.3, 0.5]}
                                colors={['#B82925', '#5C1513']}
                                style={styles.borderView}>
                                <Text style={[Font.text14HevelticaReg, { textAlign: 'center', color: 'white' }]}>
                                    {item.news_categories_name}
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    }
                    keyExtractor={item => item.id}
                />
            </View >
        )
    }
}


export default Treading
