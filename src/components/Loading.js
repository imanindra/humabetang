import React from 'react';
import { StyleSheet, View } from 'react-native';
import AnimatedLoader from "react-native-animated-loader";
import Video from 'react-native-video'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';

export default class Loader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { visible } = this.props;
    return (
      // <AnimatedLoader
      //   visible={visible}
      //   overlayColor="rgba(255,255,255,0.75)"
      //   source={require('./Loader.json')}
      //   animationStyle={styles.lottie}
      //   speed={1}
      // />
      // <View style={{ flex: 1 }}>
      // <View style={{flex : 1,backgroundColor : 'black'}}>
        <Video source={require('../assets/LOADING.mp4')}
          style={{
            // position: 'absolute',
            // top: 0,
            // left: 0,
            // right: 0,
            // bottom: 0,
            zIndex: 999,
            height : heightPercentageToDP(100),
            width : widthPercentageToDP(100)
          }}
          muted={true}
          repeat={true}
          resizeMode='cover' />
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  lottie: {
    width: 100,
    height: 100
  }
});