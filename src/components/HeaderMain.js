import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions, TextInput, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../assets'
import Constant from '../constant/constant'
import { Actions } from 'react-native-router-flux';
const { width, height } = Dimensions.get('window');

class Header extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container} elevation={1}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: wp(5) }}>
                    <Image
                        source={assets.icon_logo}
                        style={{
                            // backgroundColor: 'red',
                            width: wp(15),
                            height: hp(5),
                            alignItems: 'center',
                            resizeMode: 'contain'
                        }}
                    />


                    <Image
                        style={styles.iconSearch}
                        source={assets.icon_search}
                    />
                    <TextInput
                        style={{ width: wp(60), height: hp(5), paddingHorizontal: wp(10), borderRadius: wp(60) / 2, backgroundColor: '#E9EAEE' }}
                        placeholder={'Search'}
                    />
                    <TouchableOpacity
                        onPress={()=> Actions.Login()}
                        >
                        <Image
                            source={assets.icon_profile}
                            style={styles.iconProfile}
                        />
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: hp(7),
        backgroundColor: Constant.COLOR.WHITE,
        shadowColor: "#000000",
        justifyContent: 'center',
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    iconSearch: {
        position: 'absolute',
        left: wp(27),
        zIndex: 1,
        width: wp(5),
        height: wp(5),
        alignItems: 'center'
    },
    iconProfile: {
        width: wp(8),
        height: wp(8),
        alignItems: 'center'
    }
})

export default Header
