import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions, TextInput, TouchableOpacity } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../assets'
import Constant from '../constant/constant'
import { Actions } from 'react-native-router-flux';
const { width, height } = Dimensions.get('window');

class HeaderBack extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container} elevation={1}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: wp(5) }}>
                    <TouchableOpacity
                        onPress={()=> Actions.pop()}
                    >
                        <Image
                            source={assets.icon_back}
                            style={{
                                // backgroundColor: 'red',
                                width: wp(3),
                                height: hp(2),
                                alignItems: 'center',
                                // resizeMode: 'contain'
                            }}
                        />
                    </TouchableOpacity>


                    <Image
                        style={{
                            width: wp(20),
                            height: hp(5),
                            alignItems: 'center',
                            resizeMode: 'contain'
                        }}
                        source={assets.icon_logo}
                    />

                    <Image
                        source={assets.icon_profile}
                        style={styles.iconProfile}
                    />

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: hp(7),
        backgroundColor: Constant.COLOR.WHITE,
        shadowColor: "#000000",
        justifyContent: 'center',
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    iconSearch: {
        position: 'absolute',
        left: wp(32),
        zIndex: 1,
        width: wp(5),
        height: wp(5),
        alignItems: 'center'
    },
    iconProfile: {
        width: wp(8),
        height: wp(8),
        alignItems: 'center'
    }
})

export default HeaderBack
