import React, { Component } from 'react';
import { View, BackHandler, Image, AppRegistry, Text } from 'react-native';
import { Scene, Stack, Router, Tabs, Actions } from 'react-native-router-flux';
import Splash from './screens/SplashScreen/SplashScreen'
import Login from './screens/Login/Login';
import Home from './screens/Home/Home';
import DetailBerita from './screens/DetailBerita/DetailBerita';
import LiveStreaming from './screens/LiveStreaming/LiveStreaming';
import Life from './screens/Life/Life';
import Follow from './screens/Follow/Follow';
import ListStreaming from './screens/LiveStreaming/ListStreaming';
import assets from './assets';
import Font from './constant/Font';
import Loading from './components/Loading'

class router extends React.Component {
    state = {
        isInitiated: false,
        token: null,
    }

    render() {
        const { token, isInitiated } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <Router>
                    <Scene key="root">
                        <Scene key="Splash" initial={true} component={Splash} hideNavBar />
                        <Scene key="Login" component={Login} hideNavBar />
                        {/* <Scene key="Loading" initial={true} component={Loading} hideNavBar /> */}
                        <Scene key="DetailBerita" component={DetailBerita} hideNavBar />
                        <Scene key="LiveStreaming" component={LiveStreaming} hideNavBar />
                        <Scene
                            // initial={true}
                            tabs={true}
                            key='tabBarMain'
                            type="reset"
                            showLabel={true}
                            style={{ backgroundColor: 'white' }}

                        >
                            <Scene key="Home"
                                // initial={true}
                                backToInitial={true}
                                component={Home}
                                title="Home"
                                icon={({ focused }) =>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image
                                            style={{ width: 25, height: 25, resizeMode: 'contain' }}
                                            source={focused ? assets.icon_home_active : assets.icon_home_disable}
                                        />
                                        <Text style={[Font.text10HeltavicaReg, { alignSelf: 'center' }]}>HOME</Text>
                                    </View>
                                }

                                // <Image
                                //     style={{ width: 25, height: 25, resizeMode: 'contain' }}
                                //     source={assets.icon_home_active}
                                // />}
                                hideNavBar />

                            <Scene key="Life"
                                // initial={true}
                                component={Life}
                                title="Life"
                                icon={({ focused }) =>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image
                                            style={{ width: 25, height: 25, resizeMode: 'contain' }}
                                            source={focused ? assets.icon_paper : assets.icon_paper_disable}
                                        />
                                        <Text style={[Font.text10HeltavicaReg, { alignSelf: 'center' }]}>LIFE</Text>
                                    </View>
                                }
                                hideNavBar />

                            <Scene key="Follow"
                                // initial={true}
                                component={Follow}
                                title="Follow"
                                icon={({ focused }) =>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image
                                            style={{ width: 25, height: 25, resizeMode: 'contain' }}
                                            source={assets.icon_add_disable}
                                        />
                                        <Text style={[Font.text10HeltavicaReg, { alignSelf: 'center' }]}>FOLLOW</Text>
                                    </View>
                                }
                                hideNavBar />


                            <Scene key="TvStreaming"
                                // initial={true}
                                component={ListStreaming}
                                title="TvNetwork"
                                icon={({ focused }) =>
                                    <View style={{ alignItems: 'center' }}>
                                        <Image
                                            style={{ width: 25, height: 25, resizeMode: 'contain' }}
                                            source={assets.icon_monitor_disable}
                                        />
                                        <Text style={[Font.text10HeltavicaReg, { alignSelf: 'center' }]}>TV NETWORK</Text>
                                    </View>
                                }
                                hideNavBar />

                        </Scene>
                    </Scene>
                </Router>
            </View>

        )
    }
}

export default router;