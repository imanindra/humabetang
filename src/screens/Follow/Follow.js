import React, { Component } from 'react';
import {
    View,
    Image,
    StatusBar,
    Text
} from 'react-native';
import Video from 'react-native-video'
import HeaderBack from '../../components/HeaderBack';
import assets from '../../assets';
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import Font from '../../constant/Font';


export default class Follow extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <HeaderBack />
                <View style={{ alignSelf: 'center', flex: 1, justifyContent: 'center' }}>
                    <Image
                        style={{
                            resizeMode: 'center',
                            height: heightPercentageToDP(20),
                            width: widthPercentageToDP(70)
                        }}
                        source={assets.icon_logo}
                    />
                    <Text style={[Font.text20HevelticaReg, { textAlign: 'center' }]}>Follow Page</Text>
                </View>
            </View>
        );
    }
}
