import React, { Component } from 'react';
import {
    View,
    Image,
    StatusBar,
    Text,
    TextInput,
    StyleSheet,
    Dimensions,
    ScrollView,
    TouchableOpacity,
    Share
} from 'react-native';
import Assets from '../../assets';
import Constant from '../../constant/constant'
import { Actions } from 'react-native-router-flux'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import assets from '../../assets';
import HeaderBack from '../../components/HeaderBack';
import { getDetailNews } from '../../services/api';
import Storage from '../../data/Storage';
import HTML from 'react-native-render-html';
import moment from "moment";
import Font from '../../constant/Font';
import Color from '../../constant/Color';
import Video from 'react-native-video';
import { WebView } from 'react-native-webview';
import YoutubePlayer from 'react-native-youtube-iframe';


export default class DetailBerita extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataDetail: '',
            detailContent: '',
            playVideo: false
        };
    }

    async componentDidMount() {
        const { id_news } = this.props
        console.log('asad', id_news)
        // const token = await Storage.getToken()
        await getDetailNews(id_news).then(data => {
            this.setState({
                dataDetail: data.data,
                detailContent: data.data.news_content.rendered,
                detailAuthors: data.data.authors.name,
                news_link: data.data.news_link,
                news_img: data.data.news_img,
                news_title: data.data.news_title

            })
            console.log("::DATA", this.state.dataDetail)
        })
    }

    componentWillUnmount() {
        StatusBar.setHidden(false);
    }

    async onShare() {
        const { news_link, news_img, news_title } = this.state
        console.log(news_img)
        try {
            const result = await Share.share({
                title: news_title,
                message: news_link,
                url: news_img
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                    // let shareImage = {
                    //     title: caption,//string
                    //     message: message,//string
                    //     url: imageUrl,// eg.'http://img.gemejo.com/product/8c/099/cf53b3a6008136ef0882197d5f5.jpg',

                    // };
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    }

    render() {
        const { dataDetail, detailContent, detailAuthors, playVideo } = this.state
        return (
            <View style={{ backgroundColor: '#EAECEE', flex: 1 }}>
                <HeaderBack />
                <ScrollView style={{ flex: 1, marginTop: 5, backgroundColor: 'white' }}>
                    <View style={{ paddingHorizontal: widthPercentageToDP(5) }}>
                        <Text style={[Font.text27HevelticaBold]}>{dataDetail.news_title}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Image
                                    source={Assets.icon_profile}
                                    style={{
                                        width: widthPercentageToDP(10),
                                        height: widthPercentageToDP(10),
                                        borderRadius: widthPercentageToDP(10) / 2
                                    }}
                                />
                            </View>
                            <View style={{ marginLeft: widthPercentageToDP(2), flex: 5 }}>
                                <Text style={[Font.text13HevelticaBold]}>{detailAuthors}</Text>
                                <Text style={[Font.text10UbuntuItalic, { color: '#BD2B27' }]}>{moment(dataDetail.created_at).format('LLLL')}</Text>
                            </View>

                            {/* <View style={{ right: 0, flex: 1, justifyContent: 'center' }}>
                                <Text>
                                    Ikuti
                            </Text>
                            </View> */}
                        </View>

                        <TouchableOpacity
                            onPress={() => this.setState({
                                playVideo: !playVideo
                            })}
                        >
                            {playVideo == false ? <View>
                                <Image
                                    source={{ uri: dataDetail.news_img }}
                                    style={{
                                        // backgroundColor: 'red',
                                        width: widthPercentageToDP(90),
                                        height: heightPercentageToDP(25),
                                        marginVertical: heightPercentageToDP(2)
                                    }}
                                />
                            </View>
                                :

                                <View
                                    style={{ marginVertical: heightPercentageToDP(2) }}
                                >
                                    <YoutubePlayer
                                        height={heightPercentageToDP(25)}
                                        width={widthPercentageToDP(90)}
                                        videoId={dataDetail.news_video}
                                        play={true}
                                        onChangeState={event => console.log(event)}
                                        onReady={() => console.log("ready")}
                                        onError={e => console.log(e)}
                                        onPlaybackQualityChange={q => console.log(q)}
                                        volume={50}
                                        playbackRate={1}
                                        playerParams={{
                                            cc_lang_pref: "us",
                                            showClosedCaptions: true
                                        }}
                                    />
                                </View>
                            }
                        </TouchableOpacity>

                        {/* <Text>
                            {detailContent}
                        </Text> */}
                        <HTML html={detailContent} imagesMaxWidth={Dimensions.get('window').width} />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TextInput
                                placeholder='Beri Komentar'
                                style={{
                                    paddingHorizontal: widthPercentageToDP(5),
                                    width: widthPercentageToDP(60),
                                    borderWidth: 1,
                                    alignSelf: 'center',
                                    height: heightPercentageToDP(5),
                                    borderRadius: heightPercentageToDP(7) / 2
                                }}
                            />
                            <Image
                                source={Assets.icon_heartdisable}
                                style={styles.iconMargin}
                            />
                            <Image
                                source={Assets.icon_comment}
                                style={styles.icon}
                            />
                            <TouchableOpacity
                                onPress={() => this.onShare()}
                            >
                                <Image
                                    source={Assets.icon_share}
                                    style={styles.iconMargin}
                                />
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    iconMargin: {
        resizeMode: 'contain',
        // backgroundColor: 'red',
        width: widthPercentageToDP(8),
        height: widthPercentageToDP(8),
        // borderRadius: widthPercentageToDP(8) / 2,
        alignSelf: 'center',
        marginHorizontal: widthPercentageToDP(2)
    },
    icon: {
        resizeMode: 'contain',
        width: widthPercentageToDP(8),
        height: widthPercentageToDP(8),
        // borderRadius: widthPercentageToDP(8) / 2,
        alignSelf: 'center'
    }
})

