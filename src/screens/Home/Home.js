import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,
    TouchableOpacity,
    Linking,
    FlatList,
    RefreshControl
} from 'react-native';
import Header from '../../components/HeaderMain'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import Treading from '../../components/Treading';
import ListBerita from '../../components/ListBerita';
import CarouselHeader from '../../components/CarouselHeader';
import { connect } from 'react-redux'
import {
    fetchNews, fetchNewsFinish, fetchNewsSuccess,
    getIsFetching, getData, getIsSuccess,
} from '../../redux/newslist'
import { getNews, getSlideShow, getCategoryShow, getAds, GetCategoryShowDetail } from '../../services/api';
import Storage from '../../data/Storage';
import LinearGradient from 'react-native-linear-gradient';
import Font from '../../constant/Font';
import Loading from '../../components/Loading'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newPassword: '',
            verifyPassword: '',
            dataSlides: [],
            dataCategory: [],
            dataAds: '',
            dataNews: [],
            refreshing: false,
            load: true
        };
    }

    async componentDidMount() {
        const { id_categorys } = this.props
        // const token = await Storage.getToken()
        await getNews().then(data => {
            this.setState({
                dataNews: data.data,
                load: false
            })
        })

        // await GetCategoryShowDetail(id_categorys).then(data => {
        //     this.setState({
        //         dataNews: data.data
        //     })
        // })

        await getSlideShow().then(data => {
            this.setState({
                dataSlides: data.data
            })
            console.log('Data', data)
        })

        await getCategoryShow().then(data => {
            var newData = []
            newData.push({
                id: 0,
                news_categories_name: 'Terbaru',
                news_categories_app_img: "Terbaru",
                news_categories_web_img: '',
                news_categories_status_delete: '',
                created_at: '',
                updated_at: ''
            }),
                this.setState({
                    dataCategory: [...newData, ...data.data]
                })
        })


        await getAds().then(data => {
            this.setState({
                dataAds: data.data
            })
            console.log('Data ADSSSS', this.state.dataAds)
        })
    }

    async _onClickCategory(id_category) {
        console.log('poooooooo', id_category)
        await GetCategoryShowDetail(id_category).then(data => {
            this.setState({
                dataNews: data.data
            })
            console.log(':::ADS', this.state.dataNews)
        })
    }

    _onRefresh = () => {
        this.setState({ refreshing: true });
        getNews().then(data => {
            this.setState({
                dataNews: data.data,
                refreshing: false
            })
        })
    }

    render() {
        const { dataNews, dataSlides, dataCategory, dataAds, load } = this.state
        console.log('123123', dataNews)
        return (
            <View style={{ flex: 1, backgroundColor: '#EAECEE' }}>
                { load != true && <Header />}
                <View style={styles.headerView}>
                    {/* <Treading
                        Category={dataCategory}
                    /> */}
                    <View style={{ alignItems: 'center' }}>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={dataCategory}
                            renderItem={({ item }) =>
                                <TouchableOpacity
                                    onPress={() => this._onClickCategory(item.id)}
                                >
                                    <LinearGradient
                                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                                        locations={[0.3, 0.5]}
                                        colors={['#B82925', '#5C1513']}
                                        style={styles.borderView}>
                                        <Text style={[Font.text14HevelticaReg, { textAlign: 'center', color: 'white' }]}>
                                            {item.news_categories_name}
                                        </Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            }
                            keyExtractor={item => item.id}
                        />
                    </View >
                </View>
                <ScrollView
                    style={{ marginBottom: heightPercentageToDP(7) }}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }
                >
                    {dataAds != null &&
                        <TouchableOpacity
                            onPress={() => Linking.openURL(dataAds.ads_link)}
                        >
                            {dataAds.ads_img != null &&
                                <Image
                                    source={{ uri: dataAds.ads_img }}
                                    style={{
                                        // backgroundColor: 'red',
                                        height: heightPercentageToDP(8),
                                        width: widthPercentageToDP(100),
                                        marginBottom: heightPercentageToDP(1)
                                    }}
                                />}
                        </TouchableOpacity>}
                    <CarouselHeader
                        dataSlide={dataSlides}
                    />
                    {dataNews.length == 0 ?
                        <View style={{ alignSelf: 'center', marginTop: load == true ? 0 : heightPercentageToDP(10) }}>
                            {load == true ?
                                <Loading visible={true} /> :
                                <Text style={[Font.text20HevelticaReg]}>Berita Kosong</Text>
                            }

                        </View>
                        :
                        <ListBerita
                            dataList={dataNews}
                        />}
                </ScrollView>
            </View >
        )
    }
}

// const mapStateToProps = state => ({
//     isFetching: getIsFetching(state.games),
//     data: getData(state.games),
//     isSuccess: getIsSuccess(state.games),
//     datafilter: getDataFilter(state.games),
// })

export default Home


const styles = StyleSheet.create({
    headerView: {
        backgroundColor: 'white',
        width: widthPercentageToDP(100),
        height: heightPercentageToDP(6),
        marginVertical: heightPercentageToDP(1),
        justifyContent: 'center'
    },
    borderView: {
        padding: widthPercentageToDP(1),
        width: widthPercentageToDP(21),
        borderRadius: widthPercentageToDP(5),
        marginHorizontal: widthPercentageToDP(2),
    }
});

