import React, { Component } from 'react';
import {
    View,
    Image,
    StatusBar,
    Text,
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Linking,
    FlatList,
    Dimensions
} from 'react-native';
import Assets from '../../assets';
import Constant from '../../constant/constant'
import { Actions, ActionConst } from 'react-native-router-flux'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import assets from '../../assets';
import HeaderClose from '../../components/HeaderClose';
import Treading from '../../components/Treading';
import { getAds, getDetailTv, getCategoryShow } from '../../services/api';
import Video from 'react-native-video';
import Font from '../../constant/Font';
import HeaderStreaming from '../../components/HeaderTitleStreaming';
import { WebView } from 'react-native-webview';
import LinearGradient from 'react-native-linear-gradient';
import Orientation from "react-native-orientation";

export default class LiveStreaming extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataAds: '',
            paused: false,
            dataDetail: '',
            fullscreen: false,
            showControls: true
        };
    }

    async componentDidMount() {
        const { id } = this.props
        await getAds().then(data => {
            this.setState({
                dataAds: data.data
            })
        })
        await getDetailTv(id).then(data => {
            this.setState({
                dataDetail: data.data
            })

        })

        await getCategoryShow().then(data => {
            var newData = []
            newData.push({
                id: 0,
                news_categories_name: 'Terbaru',
                news_categories_app_img: "Terbaru",
                news_categories_web_img: '',
                news_categories_status_delete: '',
                created_at: '',
                updated_at: ''
            }),
                this.setState({
                    dataCategory: [...newData, ...data.data]
                })
        })
        // this.setState({
        //     paused : false
        // })
    }

    componentWillUnmount() {
        this.setState({
            paused: true
        })
        Orientation.lockToPortrait();
        // console.log(':::asdasdasdasd:::')
    }

    onClick() {
        Actions.Home({ id_categorys: item.id }, () => this.setState({ paused: true }))
    }

    handleFullscreen() {
        const { fullscreen } = this.state
        { fullscreen == false && Orientation.lockToLandscapeLeft() }
        { fullscreen == true && Orientation.lockToPortrait() }
        this.setState({
            fullscreen: !fullscreen
        })
        // {
        //     fullscreen != true ?
        //         Orientation.lockToPortrait() : 
        // }


    }



    render() {
        const { Title } = this.props
        const { dataAds, dataCategory, dataDetail } = this.state
        return (
            <View style={{ backgroundColor: '#F2F3F5', height: heightPercentageToDP(100) }}>
                {this.state.fullscreen == false &&
                    <HeaderStreaming
                        Title={Title}
                    />}
                {this.state.fullscreen == false && <View style={{
                    marginVertical: heightPercentageToDP(1),
                    paddingVertical: heightPercentageToDP(1),
                    backgroundColor: 'white'
                }}>
                    {/* <Treading /> */}
                    <View style={{ alignItems: 'center' }}>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={dataCategory}
                            renderItem={({ item }) =>
                                <TouchableOpacity
                                    onPress={() => this.setState({
                                        paused: true
                                    }, () => Actions.tabBarMain({ id_categorys: item.id }))}
                                >
                                    <LinearGradient
                                        start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                                        locations={[0.3, 0.5]}
                                        colors={['#B82925', '#5C1513']}
                                        style={styles.borderView}>
                                        <Text style={[Font.text14HevelticaReg, { textAlign: 'center', color: 'white' }]}>
                                            {item.news_categories_name}
                                        </Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            }
                            keyExtractor={item => item.id}
                        />
                    </View >
                </View>}

                {this.state.fullscreen == false && <TouchableOpacity
                    onPress={() => Linking.openURL(dataAds.ads_link)}
                >
                    <Image
                        source={{ uri: dataAds.ads_img }}
                        style={{
                            // backgroundColor: 'red',
                            height: heightPercentageToDP(8),
                            width: widthPercentageToDP(100),
                            marginBottom: heightPercentageToDP(1)
                        }}
                    />
                </TouchableOpacity>}
                <TouchableOpacity
                    onPress={() => this.handleFullscreen()}
                    style={{
                        // backgroundColor: 'green',
                        // width: widthPercentageToDP(100),
                        // height: heightPercentageToDP(25)
                    }}>
                    <Video source={{ uri: dataDetail.tv_url_stream }}   // Can be a URL or a local file.
                        ref={(ref) => {
                            this.player = ref
                        }}
                        paused={this.state.paused}               // Store reference
                        onBuffer={this.onBuffer}                // Callback when remote video is buffering
                        onError={this.videoError}               // Callback when video cannot be loaded
                        style={this.state.fullscreen ? styles.fullscreenVideo : styles.video}
                        fullscreenAutorotate={true}
                        resizeMode={'contain'}
                    />
                    {/* <TouchableOpacity

                        onPress={() => this.handleFullscreen()}
                        style={{
                            position: 'absolute',
                            right: 10,
                            bottom: 10,
                            height: 25,
                            width: 25
                        }}
                    >
                        <Image
                            source={assets.icon_zoom}
                            style={{
                                height: 25,
                                width: 25
                            }}
                        />
                    </TouchableOpacity> */}
                </TouchableOpacity>
                {this.state.fullscreen == false && <View style={{ backgroundColor: '#B82925', paddingVertical: heightPercentageToDP(1), flexDirection: 'row' }}>
                    <Image
                        style={{ width: widthPercentageToDP(5), height: widthPercentageToDP(5), resizeMode: 'contain', paddingHorizontal: widthPercentageToDP(5) }}
                        source={Assets.icon_livechat}
                    />
                    <Text style={Font.text16HevelticaReg, { color: 'white' }}>LIVE CHAT</Text>
                </View>}
                {this.state.fullscreen == false && <Image
                    style={{
                        alignSelf: 'center',
                        width: widthPercentageToDP(70),
                        height: heightPercentageToDP(10),
                        resizeMode: 'contain',
                        position: 'absolute',
                        bottom: heightPercentageToDP(5)
                    }}
                    source={Assets.icon_camarTv}
                />}
            </View >
        );
    }


}

var styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    headerView: {
        backgroundColor: 'white',
        width: widthPercentageToDP(100),
        height: heightPercentageToDP(6),
        marginVertical: heightPercentageToDP(1),
        justifyContent: 'center'
    },
    borderView: {
        padding: widthPercentageToDP(1),
        width: widthPercentageToDP(21),
        borderRadius: widthPercentageToDP(5),
        marginHorizontal: widthPercentageToDP(2),
    },
    container: {
        flex: 1,
        backgroundColor: '#ebebeb',
    },
    video: {
        height: Dimensions.get('window').width * (9 / 16),
        width: Dimensions.get('window').width,
        backgroundColor: 'black',
    },
    fullscreenVideo: {
        height: Dimensions.get('window').width,
        width: Dimensions.get('window').height,
        backgroundColor: 'black',
    },
    text: {
        marginTop: 30,
        marginHorizontal: 20,
        fontSize: 15,
        textAlign: 'justify',
    },
    fullscreenButton: {
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center',
        paddingRight: 10,
    },
    controlOverlay: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: '#000000c4',
        justifyContent: 'space-between',
    },
});