import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Alert,
    ScrollView,
    FlatList,
    Dimensions,
} from 'react-native';
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import Color from '../../constant/Color';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import { getListTv } from '../../services/api';
import moment from "moment";
import Font from '../../constant/Font';
import assets from '../../assets';
import HeaderStreaming from '../../components/HeaderStreaming';

export default class ListBerita extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    clickEventListener(item) {
        Alert.alert(item.title)
    }

    async componentDidMount() {
        await getListTv().then(data => {
            this.setState({
                dataListTv: data.data
            })
        })
        // this.setState({
        //     paused : false
        // })
    }

    render() {
        const { dataListTv } = this.state
        console.log('HAJSHJASH', dataListTv)
        return (
            <View style={{ flex: 1, backgroundColor: '#EAECEE' }}>
                <HeaderStreaming
                    Title={'Tv Network'}
                />

                <FlatList style={styles.list}
                    contentContainerStyle={styles.listContainer}
                    data={dataListTv}
                    horizontal={false}
                    keyExtractor={(item) => {
                        return item.id;
                    }}
                    renderItem={({ item }) => {
                        return (
                            <View style={{ marginBottom: heightPercentageToDP(1) }}>
                                <TouchableOpacity
                                    style={[styles.card]}
                                    onPress={() => Actions.LiveStreaming({ id: item.id, Title: item.tv_name })}
                                >
                                    <Image
                                        style={styles.cardImage}
                                        source={{ uri: item.tv_img }} />
                                </TouchableOpacity>
                            </View>
                        )
                    }} />
                <Image
                    style={{
                        alignSelf: 'center',
                        width: widthPercentageToDP(70),
                        height: heightPercentageToDP(10),
                        resizeMode: 'contain',
                        position: 'absolute',
                        bottom: heightPercentageToDP(7)
                    }}
                    source={assets.icon_camarTv}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: heightPercentageToDP(1),
        width: widthPercentageToDP(95),
        alignSelf: 'center',
        flex: 1
    },
    list: {
        paddingVertical: heightPercentageToDP(2),
        paddingHorizontal: widthPercentageToDP(5),
        // backgroundColor: "#E6E6E6",
    },

    card: {
        flexDirection: 'row',
        borderRadius: widthPercentageToDP(1),
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        backgroundColor: 'white',
        elevation: 2,
        flex: 1,
        paddingVertical: heightPercentageToDP(1)
    },
    cardImage: {
        borderRadius: widthPercentageToDP(1.5),
        height: widthPercentageToDP(20),
        width: widthPercentageToDP(85),
        resizeMode: 'stretch',
        alignSelf: 'center',
        marginHorizontal: widthPercentageToDP(2)
    },
    title: {
        fontSize: 20,
        flex: 1,
        color: Color.COLOR.BLACK,
        fontWeight: 'bold',
        // marginLeft: 40
    },
    subTitle: {
        fontSize: 12,
        flex: 1,
        color: "#FFFFFF",
    },
    icon: {
        height: 20,
        width: 20,
    }
});
