import React, { Component } from 'react';
import {
  View,
  Image,
  StatusBar,
  Text
} from 'react-native';
import Assets from '../../assets';
import Constant from '../../constant/constant'
import { Actions } from 'react-native-router-flux'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import assets from '../../assets';
import Video from 'react-native-video'
import Font from '../../constant/Font';


export default class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    StatusBar.setHidden(true);
    setTimeout(function () {
      Actions.tabBarMain();
    }, 6000)
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', width: null, height: null, backgroundColor: 'black' }}>
        <Video source={require('../../assets/SplashScreen.mp4')}
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          }}
          muted={true}
          repeat={true}
          resizeMode="contain" />
        <Text style={[Font.text16HevelticaBold, { color: 'white', position: 'absolute',bottom : 20, right: 20 }]}>V 1.1</Text>
      </View>
    );
  }
}
