import { StyleSheet, Dimensions } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Color from './Color';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    text10HeltavicaReg : {
        fontSize: wp(2.5),
        color: Color.COLOR.BLACK,
        fontFamily: 'Helvetica'
    },
    text14HevelticaReg: {
        fontSize: wp(3),
        color: Color.COLOR.BLACK,
        fontFamily: 'Helvetica'
    },
    text16HevelticaReg: {
        fontSize: wp(5),
        color: Color.COLOR.BLACK,
        fontFamily: 'Helvetica'
    },
    text20HevelticaReg: {
        fontSize: wp(5),
        color: Color.COLOR.BLACK,
        fontFamily: 'Helvetica'
    },
    text13HevelticaBold: {
        fontSize: wp(4),
        color: Color.COLOR.BLACK,
        fontFamily: 'Helvetica-Bold'
    },
    text16HevelticaBold: {
        fontSize: wp(5),
        color: Color.COLOR.BLACK,
        fontFamily: 'Helvetica-Bold'
    },
    text27HevelticaBold: {
        fontSize: wp(6),
        color: Color.COLOR.BLACK,
        fontFamily: 'Helvetica-Bold'
    },
    text10UbuntuItalic: {
        fontSize: 10,
        color: Color.COLOR.BLACK,
        fontFamily: 'Ubuntu-L'
    }
})
