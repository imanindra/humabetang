export default {
    COLOR: {
        PRIMARY :'#005D9E',
        SECONDARY: "#1A425A",
        WHITE: "white",
        BLACK : 'black',
        TEXTINPUT : '#F4F4F4'
    },
}