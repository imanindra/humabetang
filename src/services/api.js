import { store } from '../configureStore';
import {
    defaultHeaders,
    buildResponse,
    buildResponseNew,
    buildHeaders,
    buildParam,
    BASE_URL,
    buildResponseLogin,
    buildHeadersImage,
} from './apiHelper';

import { ERROR_TAG } from '../redux/error';
import DeviceInfo from 'react-native-device-info';

let uniqueId = DeviceInfo.getUniqueId();

// // Login
// export function login(
//     username,
//     password
// ) {
//     return fetch(`${BASE_URL}login`, {
//         headers: defaultHeaders,
//         method: 'POST',
//         body: JSON.stringify({
//             username,
//             password,
//         }),
//     })
//         .then(response => buildResponse(response, ERROR_TAG.ERROR_LOGIN))
//         .then(data => data);
// }

// Logout
export function logout(accessToken) {
    return fetch(`${BASE_URL}logout`, {
        headers: buildHeaders(accessToken),
    })
        .then(response => buildResponse(response, ERROR_TAG.ERROR_LOGOUT))
        .then(data => data);
}

// GetNews
export function getNews() {
    return fetch(`${BASE_URL}news`, {
        headers: defaultHeaders
    })
        .then(response => buildResponse(response, "ERROR NEWS LIST"))
        .then(data => data)
}

// setView
export function setView(
    id_news
) {
    return fetch(`${BASE_URL}guest/set/views`, {
        headers: defaultHeaders,
        method: 'POST',
        body: JSON.stringify({
            id_news,
        }),
    })
        .then(response => buildResponse(response, 'ERROR SET VIEWS'))
        .then(data => data);
}

// GetDetailNews
export function getDetailNews(id_news) {
    return fetch(`${BASE_URL}detail-news/{id_news}?id_news=` + id_news, {
        headers: defaultHeaders
    })
        .then(response => buildResponse(response, "ERROR NEWS LIST"))
        .then(data => data)
}

// GetComment
export function getComment(id_news) {
    return fetch(`${BASE_URL}comment/news/{id_news}?id_news=` + id_news, {
        headers: defaultHeaders
    })
        .then(response => buildResponse(response, "ERROR GET COMMENT"))
        .then(data => data)
}

// GetCategoryShow
export function getCategoryShow(accessToken) {
    return fetch(`${BASE_URL}news/category`, {
        headers: buildHeaders(accessToken)
    })
        .then(response => buildResponse(response, "ERROR GET CATEGORY"))
        .then(data => data)
}

// GetCategoryShowDetail
export function GetCategoryShowDetail(id_category) {
    return fetch(`${BASE_URL}news/category/{id_category}?id_category=` + id_category, {
        headers: defaultHeaders
    })
        .then(response => buildResponse(response, "ERROR GET CATEGORY"))
        .then(data => data)
}


// GetSlideShow
export function getSlideShow(accessToken) {
    return fetch(`${BASE_URL}slideshow`, {
        headers: buildHeaders(accessToken)
    })
        .then(response => buildResponse(response, "ERROR SLIDE SHOW"))
        .then(data => data)
}

// GetListTv
export function getListTv() {
    return fetch(`${BASE_URL}tv`, {
        headers: defaultHeaders
    })
        .then(response => buildResponse(response, "ERROR LIST TV"))
        .then(data => data)
}

// GetListTvDetail
export function getDetailTv(idTv) {
    return fetch(`${BASE_URL}detail-tv/{id_tv}?id_tv=` + idTv, {
        headers: defaultHeaders
    })
        .then(response => buildResponse(response, "ERROR DETAIL TV"))
        .then(data => data)
}


// GetIklan
export function getAds() {
    return fetch(`${BASE_URL}ads`, {
        headers: buildHeaders
    })
        .then(response => buildResponse(response, "ERROR ADS"))
        .then(data => data)
}

// LoginApp
export function login(
    token,
) {
    return fetch(`${BASE_URL}login`, {
        headers: defaultHeaders,
        method: 'POST',
        body: JSON.stringify({
            token,
        }),
    })
        .then(response => buildResponse(response, 'Error Login'))
        .then(data => data);
}

// SetFirebase
export function setFirebase(
    users_id,
    fcm_token,
) {
    return fetch(`${BASE_URL}firebase/token`, {
        headers: defaultHeaders,
        method: 'POST',
        body: JSON.stringify({
            users_id,
            fcm_token,
            device_id: uniqueId,
        }),
    })
        .then(response => buildResponse(response, 'Error Firebase'))
        .then(data => data);
}




