import { Actions } from 'react-native-router-flux';
import { store } from '../configureStore';
import Storage from '../data/Storage';
import { ERROR_TAG, receiveError, DEFAULT_ERROR_MESSAGE } from '../redux/error';

export const SITE_URL = 'https://humabetang.com/';
// export const PRODUCTION_URL = 'https://member.whitesand88.com/'
// export const ACCOUNTING_URL = 'https://accounting.ws.sgapi.net/'
// export const SITE_URL = 'http://192.168.1.21:8000/';
export const BASE_URL = `${SITE_URL}api/v1/`;
export const defaultHeaders = {
  'Content-Type': 'application/json',
};

export const multiPart = {
  'Content-Type': 'multipart/form-data',
};

export function buildHeaders(accessToken) {
  return {
    ...defaultHeaders,
    Authorization: `Bearer ${accessToken}`,
    Accept: 'application/json',
  };
}

export function buildHeadersImage(accessToken) {
  return {
    ...multiPart,
    Authorization: `Bearer ${accessToken}`,
    // Accept: 'application/json',
  };
}

export function buildParam(param) {
  const formattedParam = param.map(value => ({
    [value[0]]: value[1],
  }));
  return JSON.stringify(formattedParam);
}

export function buildResponse(response, errorTag) {
  console.log('------',response)
  if (!response.ok) {
    if (response.status === 401) {
      Actions.reset('Login')
    }
    store.dispatch(
      receiveError({
        errorCode: response.status,
        errorTag: errorTag,
        errorMessage: '',
        defaultMessage: 'Failed',
      }),
    );
  }
  return response.json();
}

export function buildResponseNew(response) {
  if (!response.ok) {
    if (response.status === 401) {
      Actions.reset('Login')
    }
  }
  return response.json();
}

export function buildResponseLogin(response, errorTag) {
  if (!response.ok) {
    if (response.status === 401 || response.status === 400) {
      store.dispatch(
        receiveError({
          errorCode: response.status,
          errorTag: errorTag,
          defaultMessage: DEFAULT_ERROR_MESSAGE.DEFAULT_MESSAGE_1,
        }),
      );
    } else {
      store.dispatch(
        receiveError({
          errorCode: response.status,
          errorTag: errorTag,
          errorMessage: DEFAULT_ERROR_MESSAGE.ERROR_MESSAGE_500,
          defaultMessage: DEFAULT_ERROR_MESSAGE.DEFAULT_MESSAGE_2,
        }),
      );
    }
  }
  return response.json();
}
