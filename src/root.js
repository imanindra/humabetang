import React from 'react';
import { SafeAreaView, Alert, BackHandler, View, Text, StyleSheet, TouchableOpacity, Image, AppRegistry, Linking } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import AppRouter from './router';
import { store, persistor } from './configureStore';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from "react-native-firebase";
import { setFirebase } from './services/api';
import Storage from './data/Storage';

console.disableYellowBox = true;

class root extends React.Component {
    state = {
        isModalVisible: false,
        active: true,
        userid: 0
    };

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    async componentDidMount() {
        this.checkPermission();
        this.createNotificationListeners();
        setTimeout(() => {
            this.onFire()
        }, 10000);
    }


    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        // If Premission granted proceed towards token fetch
        if (enabled) {
            this.getToken();
        } else {
            // If permission hasn’t been granted to our app, request user in requestPermission method. 
            this.requestPermission();
        }
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
    }

    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }

    async createNotificationListeners() {

        // This listener triggered when notification has been received in foreground
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            const { title, body } = notification;
            this.displayNotification(title, body);
        });

        // This listener triggered when app is in backgound and we click, tapped and opened notifiaction
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const { title, body } = notificationOpen.notification;
            this.displayNotification(title, body);
        });

        // This listener triggered when app is closed and we click,tapped and opened notification 
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            console.log('::DATA', notificationOpen)
            const { title, body } = notificationOpen.notification;
            this.displayNotification(title, body);
        }
    }


    displayNotification(title, body) {
        // we display notification in alert box with title and body
        // Alert.alert(
        //     title, body,
        //     [
        //         { text: 'Ok', onPress: () => console.log('ok pressed') },
        //     ],
        //     { cancelable: false },
        // );
    }

    async componentWillMount() {
        this.setState({
            isModalVisible: false
        })
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible, isLoading: false });
    };

    async onFire() {
        const User = await Storage.getUser()
        // this.setState
        // console.log('::USER::', User.id)
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        // console.log(':::FCMTOKEN::::', fcmToken)
        // await setFirebase(users_id = User.id == '' ? this.state.userid : User.id, fcm_token = fcmToken).then(data => {
        //     console.log(':::FCMTOKEN123123::::', fcmToken)
        //     this.setState({
        //         dataNe: data
        //     })

        // })
        // {
        //     User.id == null ?
        await setFirebase(this.state.userid, fcmToken).then(data => {
            // console.log(':::FCMTOKEN123123::::', data)
            this.setState({
                dataNe: data
            })
        })
        //      :

        //     await setFirebase(User.id, fcmToken).then(data => {
        //         console.log(':::FCMTOKEN123123::::', data)
        //         this.setState({
        //             dataNe: data
        //         })
        //     })
        // }
    }

    render() {
        const { isModalVisible } = this.state
        return (
            <Provider store={store} >
                <PersistGate persistor={persistor}>
                    <SafeAreaView style={{ flex: 1, backgroundColor: '#FFF' }}>
                        <AppRouter />
                    </SafeAreaView>
                </PersistGate>
            </Provider>
        );
    }
}

export default root;
AppRegistry.registerComponent('root', () => root);

