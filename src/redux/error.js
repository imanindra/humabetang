import { Map } from 'immutable';

export const ERROR_TAG = {
  ERROR_REGISTER: 'ERROR_REGISTER',
  ERROR_LOGIN: 'ERROR_LOGIN',
  ERROR_LOGOUT: 'ERROR_LOGOUT',
  ERROR_VIEW_PROFILE: 'ERROR_PROFILE',
  ERROR_UPDATE_PROFILE: 'ERROR_UPDATE_PROFILE',
  ERROR_CHANGE_PASSWORD: 'ERROR_CHANGE_PASSWORD',
};

export const RECEIVE_ERROR = 'error/receiveError';
export const DISMISS_ERROR = 'error/dismissError';
export const RESET_ERROR = 'error/resetError';
export const SET_ERROR_MESSAGE = 'error/setErrorMessage';
export const SET_STATUS_MESSAGE = 'error/setStatusMessage';
export const SET_MODAL_MESSAGE = 'error/setModalMessage';
export const DISMISS_ERROR_MESSAGE = 'error/dismissMessage';

export function receiveError(payload) {
  return {
    type: RECEIVE_ERROR,
    payload,
  };
}

export function dismissError() {
  return {
    type: DISMISS_ERROR,
  };
}

export function resetError() {
  return {
    type: RESET_ERROR,
  };
}

export function setErrorMessage(payload) {
  return {
    type: SET_ERROR_MESSAGE,
    payload,
  };
}

export function dismissMessage(){
  return {
    type: DISMISS_ERROR_MESSAGE,
  };
}

export function setStatusMessage(payload) {
  return {
    type: SET_STATUS_MESSAGE,
    payload,
  };
}


const initialState = Map({
  hasError: false,
  errorCode: '',
  errorTag: '',
  errorType: '',
  errorMessage: '',
  defaultMessage: '',
  isModalVisible: false,
  isImageMessage: '',
  isStatusMessage: '',
  isMessageModal: '',
});

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case RECEIVE_ERROR:
      return state
        .set('hasError', true)
        .set('errorCode', action.payload.errorCode)
        .set('errorTag', action.payload.errorTag)
        .set('errorMessage', action.payload.errorMessage)
        .set('defaultMessage', action.payload.defaultMessage);
    case DISMISS_ERROR:
      return state.withMutations(currentState =>
        currentState.set('hasError', false),
      );
    case RESET_ERROR:
      return state
        .set('hasError', false)
        .set('errorCode', '')
        .set('errorMessage', '')
        .set('errorTag', '');
    case SET_ERROR_MESSAGE:
      return state.withMutations(currentState =>
        currentState.set('errorMessage', action.payload),
      );

    case SET_STATUS_MESSAGE:
      return state.withMutations(currentState =>
        currentState.set('isStatusMessage', action.payload),
      );

    case DISMISS_ERROR_MESSAGE:
      return state.withMutations(currentState =>
        currentState.set('isModalVisible', false),
      );
    case SET_MODAL_MESSAGE:
      return state.withMutations(currentState =>
        currentState.set('isMessageModal', action.payload),
      );
    default:
      return state;
  }
}

export const getHasError = state => state.get('hasError');
export const getErrorCode = state => state.get('errorCode');
export const getErrorMessage = state => state.get('errorMessage');
export const getDefaultMessage = state => state.get('defaultMessage');
export const getErrorTag = state => state.get('errorTag');
export const getImageMessage = state => state.get('isImageMessage')
export const getStatusMessage = state => state.get('isStatusMessage');
export const getModalMessage = state => state.get('isMessageModal');
export const getModalVisible = state => state.get('isModalVisible')
