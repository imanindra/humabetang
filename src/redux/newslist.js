import { Map } from 'immutable';

export const FETCH_NEWS= 'gamelist/fetchNews'
export const FETCH_NEWS_START = 'gamelist/fetchNewsStart'
export const FETCH_NEWS_SUCCESS = 'gamelist/fetchNewsSuccess'
export const FETCH_NEWS_FILTER = 'gamelist/fetchNewsFilter'
export const FETCH_NEWS_FINISH = 'gamelist/fetchNewsFinish'

export function fetchNews() {
    return {
        type: FETCH_NEWS,
    }
}

export function fetchNewsStart() {
    return {
        type: FETCH_NEWS_START,
    }
}

export function fetchNewsSuccess(payload) {
    return {
        type: FETCH_NEWS_SUCCESS,
        payload
    }
}

export function fetchNewsFinish() {
    return {
        type: FETCH_NEWS_FINISH
    }
}

const initialState = Map({
    isFetching: false,
    isSuccess: false,
    data: [],
    datafilter: []
})

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_NEWS_START:
            return state.set('isFetching', true);
        case FETCH_NEWS_SUCCESS:
            return state.withMutations(currentState =>
                currentState
                    .set('isFetching', false)
                    .set('isSuccess', true)
                    .set('data', action.payload)
                    .set('datafilter', action.payload),
            );
        case FETCH_NEWS_FINISH:
            return state.withMutations(currentState =>
                currentState.set('isFetching', false).set('isSuccess', false),
            );

        default:
            return state;
    }
}

export const getIsFetching = state => state.get('isFetching');
export const getData = state => state.get('data');
export const getIsSuccess = state => state.get('isSuccess');
