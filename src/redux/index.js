import {combineReducers} from 'redux';
import authReducer from './auth';
import errorReducer from './error';
import newsReducer from './newslist'

const reducers = combineReducers({
  auth: authReducer,
  error: errorReducer,
  news : newsReducer
});

export default reducers;
